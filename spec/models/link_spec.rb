require 'spec_helper'

describe "Link" do
  before { @link = build(:link) }
  subject { @link }
  it { should respond_to(:original_link) }
  it { should respond_to(:alias_link) }
  it { should respond_to(:destroy_at) }

  it { should be_valid }

  describe "when original_link is not present" do
    before { @link.original_link = "" }
    it { should_not be_valid }
  end

  describe "when original_link format is valid" do
    it "should be valid" do
      original_link = %w[https://encrypted.google.com/#safe=off&q=omg 
        https://habrahabr.ru/post/188046/ 
        people.onliner.by/2016/02/10/london/ ]
      original_link.each do |valid_original_link|
        @link.original_link = valid_original_link
        expect(@link).to be_valid
      end
    end
  end

  describe "when original_link format is invalid" do
    it "should be invalid" do
      original_link = %w[wwww.dce53d0 www.c451165a16b-tu 84677._com 14__92c12]
      original_link.each do |invalid_original_link|
        @link.original_link = invalid_original_link
        expect(@link).to be_invalid
      end
    end
  end


  describe ".update_destroy_date" do
    before { @link = create(:link) }
    it { @link.destroy_at.to_date.should eq 7.days.from_now.to_date }
  end

end
