require 'spec_helper'

  describe "Main page" do
    subject { page }
    describe "should have the right content on the layout" do
      before { visit root_path }
      text = /Shorter На этой странице Вы можете сделать из длинной**/
      it { should have_content(text) }
      it { should have_title('Shooter') }
    end
end