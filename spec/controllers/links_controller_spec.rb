require 'rails_helper'

RSpec.describe LinksController, type: :controller do

  describe "POST #create" do
    it "redirect to root_path" do
      post :create, link: attributes_for(:link, original_link: "http://foo.bar")
      expect(response).to redirect_to (root_path)
    end
    it "render a 'main' template when a new link has wrong parameter" do
      post :create, link: attributes_for(:link, original_link: nil)
      expect(response).to render_template(:main)
    end
  end

  describe "GET #router" do
    it "redirects to an original link" do
      link = create(:link)
      get :router, attributes_for(:link, alias_link: link.alias_link)
      expect(response).to redirect_to "https://habrahabr.ru/post/188046/"
    end
  end

end
