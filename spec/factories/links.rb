FactoryGirl.define do
  factory :link do
    original_link "https://habrahabr.ru/post/188046/"
    alias_link (('A'..'Z').to_a + ('a'..'z').to_a + (0..9).to_a.map(&:to_s)).sample(5).join
    # destroy_at "2016-02-09 14:08:01"
  end
end
