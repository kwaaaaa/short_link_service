class CreateLinks < ActiveRecord::Migration
  def change
    create_table :links do |t|
      t.string :original_link, limit: 500, null: false
      t.string :alias_link, null: false
      t.datetime :destroy_at
    end
  end
end
