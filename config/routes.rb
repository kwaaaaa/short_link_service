Rails.application.routes.draw do
  root 'static_pages#main'
  post '/', to: 'links#create'
  get '/:alias_link', to: 'links#router'
end
