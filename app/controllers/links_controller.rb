class LinksController < ApplicationController

  def create
    @link = Link.new(permit_params)
    if @link.save
      flash[:success] = "#{request.host}/#{@link.alias_link}"
      redirect_to root_path
    else
      flash.now[:error] = "Указана некорректная ссылка."
      render 'static_pages/main'
    end
  end

  def router
    if @link = Link.find_by_alias_link(params[:alias_link])
      redirect_to @link.original_link
    else
      redirect_to root_path
    end
  end

  private

  def permit_params
    params.require(:link).permit(:original_link)
  end

end
