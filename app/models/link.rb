class Link < ActiveRecord::Base
  validates :original_link, :alias_link, presence: true
  validates :alias_link, uniqueness: true

  VALID_ORIGINAL_LINK_REGEX = /\A(.*)([a-z0-9а-я]+)\.([a-zа-я]{2,})(\/.*)?\z/i

  validates :original_link, format: { with: VALID_ORIGINAL_LINK_REGEX }

  before_validation :create_link, if: :new_record?

  private

    def create_link
      create_alias_link
      update_destroy_date
    end

    def create_alias_link
      self.alias_link = (('A'..'Z').to_a + ('a'..'z').to_a + (0..9).to_a.map(&:to_s)).sample(5).join
    end

    def update_destroy_date
      self.destroy_at = 7.days.from_now
    end

end
